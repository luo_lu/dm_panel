#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <malloc.h>
#include <pthread.h>
#include <errno.h>
#include <linux/input.h>
#include <stdlib.h>
#include <linux/types.h>
#include <termios.h>
#include <signal.h>


#include "ar_common_define.h"
#include "ar_meta_define.h"
#include "ar_multimedia_base.h"
#include "icc_proxy_rpc_def.h"
#include "libremote_i2c.h"

int icc_proxy_rpc_to_linux_open_dev(const char *name);
int icc_proxy_rpc_to_linux_acquire_buffer(int fd,struct icc_proxy_rpc_to_linux_element_t *buffer_info);
int icc_proxy_rpc_to_linux_release_buffer(int fd,struct icc_proxy_rpc_to_linux_element_t *usr_buffer);
int icc_proxy_rpc_to_linux_exit(int usr_fd);

typedef struct {
    ar_facotry_element_obj_t pipeline;
    ar_facotry_element_obj_t audio_out;
	ar_facotry_element_obj_t aac_decoder;
	ar_facotry_element_obj_t i2s_sink;
	ar_facotry_element_obj_t tee;
}ipcam_audio_out_context_t;

typedef struct {
    ar_facotry_element_obj_t pipeline;
    ar_facotry_element_obj_t audio_in;
    ar_facotry_element_obj_t aac_encoder;
    ar_facotry_element_obj_t i2s_src;
}ipcam_audio_in_context_t;


unsigned char general_i2c_write(unsigned char dev_i2c, unsigned char dev_addr, unsigned char *data, int len)
{
        
    int fd;
    int ret;
    int i;
    unsigned char buf[64];
    
    fd = remote_i2c_open("/dev/remote_i2c");
    if(fd == 0)
    {   
        printf("remote open err\n");
        return -1;
    }
    
    printf("remote I2C config dev-i2c = %d i2c-addr = %02x, rbyte = %d\n",dev_i2c,dev_addr,len);
    ret = remote_i2c_config(fd, dev_i2c, REMOTE_I2C_MASTER_MODE, dev_addr, REMOTE_I2C_STANDARD_SPEED);
    if(ret < 0)
    {   
        printf("remote config(rd) err\n");
        return -1;
    }

    //buf[0] = sub_addr;
    memcpy(&buf[0], data, len);
    ret = remote_i2c_write(fd,buf,len);
    if (ret < 0)
    {
        printf("remote i2c write2 error!\n");
        remote_i2c_close(fd);
        return -1;
    }
    printf("write ok\n");
    remote_i2c_close(fd);
    //usleep(1000);
    return 0;
    
}

unsigned char general_i2c_read(unsigned char dev_i2c, unsigned char dev_addr, unsigned char *data, int len)
{
    
    int fd;
    int ret;
    unsigned char buf[64] = {0};
    int i;
    
    fd = remote_i2c_open("/dev/remote_i2c");
    if(fd == 0)
    {   
        printf("remote open err\n");
        return -1;
    }
    
    printf("remote I2C config dev-i2c = %d i2c-addr = %02x, rbyte = %d\n",dev_i2c,dev_addr,len);
    ret = remote_i2c_config(fd, dev_i2c, REMOTE_I2C_MASTER_MODE, dev_addr, REMOTE_I2C_STANDARD_SPEED);
    if(ret < 0)
    {   
        printf("remote config(wr) err\n");
        return -1;
    }
    
    ret = remote_i2c_read(fd, buf, len);
    if (ret < 0)
    {
        printf("remote i2c read error!\n");
        close(fd);
        return -1;
    }
    //memcpy(data, buf, len);
	for(i=0;i<len;i++){
		printf("%02x ",buf[i]);
	}
	printf("\n");
    
    remote_i2c_close(fd);
    //usleep(1000);
    return 0;
      
}

static void msleep(int msec)
{
    usleep(msec * 1000);
}



int main(int argc, char *argv[])
{

    if(argc < 3){
        printf("./test_vcm dev_i2c r|w [reg1 reg2](0x11 0x22)\n");
        return 0;
    }
    printf("%s %s %s\n",argv[0],argv[1],argv[2]);
    int dev_i2c = atoi(argv[1]);
    if (strcmp(argv[2], "r") == 0){
        general_i2c_read(dev_i2c,0x0c,NULL,2);
    }
    else if (strcmp(argv[2], "w") == 0){
        printf("wpra %s %s\n",argv[3],argv[4]);
        int reg1,reg2;
		sscanf(argv[3],"0x%x",&reg1);
		sscanf(argv[4], "0x%x", &reg2);
        printf("reg [0x%x 0x%x] \n", reg1,reg2);
        unsigned char data[2];
        data[0] = reg1;
        data[1] = reg2;
        printf("%d %02x %02x\n",dev_i2c,data[0],data[1]);
        general_i2c_write(dev_i2c,0x0c,data,2);
    }
	
	return 0;
}


