/****************************************************************************
 * Copyright (C) 2019 Shanghai Artosyn Microelectronics Limited.            *
 ****************************************************************************/
/** \addtogroup bsp 
 *  @{
 */

/**
 * @file libi2c.h
 * @author Artosyn
 * @date 9 May 2019
 * @brief File containing the APIs to do i2c operation.
 *        Users can refer to app/artosyn/usr_test/test_i2c/test_i2c.c for sample code, or contact Artosyn for help.
 * @example <app/artosyn/usr_test/test_i2c/test_i2c.c>
 */


#ifndef __SIRIUS_LIBI2C_H__
#define __SIRIUS_LIBI2C_H__

#include <linux/types.h>

#define AR_I2C_TYPE_UNKNOWN	0
//i2c reg address 7bit
#define AR_I2C_TYPE_8BIT_ADDR	1
//i2c reg address 10bit
#define AR_I2C_TYPE_16BIT_ADDR 	2
#define AR_I2C_MAX_PORT	4

/**
* @brief  i2c read/write data struct
*/
struct ar_usr_i2c
{
	__u16 ic_addr;	/**< i2c ic address */
	__u16 reg_addr;	/**< i2c reg address */
	__u16 date_len; /**< data length */
	__u8 *data; /**< read/write data point */
	int type; /**< i2c ic address length */
	int port; /**< i2c component */
};

/**
* @brief  i2c write
* @param  usr_i2c  usr command struct
* @return 0	ok.
*         -1 error.
* @note null.
*/
int ar_i2c_write_byte(struct ar_usr_i2c *usr_i2c);
int ar_i2c_write_word(struct ar_usr_i2c *usr_i2c);

/**
* @brief  i2c read
* @param  usr_i2c  usr command struct
* @return 0	ok.
*         -1 error.
* @note null.
*/
int ar_i2c_read_byte(struct ar_usr_i2c *usr_i2c);
int ar_i2c_read_word(struct ar_usr_i2c *usr_i2c);

/**
* @brief  initialize i2c component
* @param  i2c_port  i2c component
* @return 0	ok.
*         -1 error.
* @note null.
*/
int ar_i2c_init(int i2c_port);

#endif //__SIRIUS_LIBI2C_H__
/** @}*/
