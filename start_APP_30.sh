#!/bin/sh


# /etc/init.d/start_$gAPP.sh $gAPP $flashtype $a7rtos_part \
#	$ceva0_part $ceva1_part $ceva2_part $ceva3_part

mkdir -p /database
mount -t ext4 /dev/mmcblk0p27 /database
mountpoint /database
if [ $? == 1 ]
then
    /etc/mkfs.ext4 /dev/mmcblk0p27
    mount -t ext4 /dev/mmcblk0p27 /database
fi

echo "start the sceniaro for" $1

export DBUS_SESSION_BUS_ADDRESS="unix:path=/tmp/dbus-artosyn"
export PATH="/bin:/sbin:/usr/bin:/usr/sbin:/local/usr/bin/:."
export LD_LIBRARY_PATH="/tmp:/lib:/usr/lib:/local/usr/lib:"

echo gAPP=$1 flashtype=$2 a7rtos_part=$3 \
	 ceva0_part=$4 ceva1_part=$5 ceva2_part=$6 ceva3_part=$7

let tmp=`devmem 0x6050c100`
let mask=0x20200000
let val=$(($tmp|$mask))
#devmem 0x6050c100 32 $val
devmem 0x6050c104 32 0x00000400
devmem 0x64568520 32 0x0f090009 #ddr vol
#boot a7 rtos

cp /local/usr/bin/boot_assist /tmp
cp /local/usr/lib/libcjson.so /tmp
cp /local/usr/lib/libcjson.so.1 /tmp
cp /local/usr/lib/libcjson.so.1.7.8 /tmp 
cp /local/usr/bin/fw_memory_layout.json /tmp

cp /usrdata/usr/data/arstack/ar_adc_test /tmp

cp /local/usr/bin/*.dtb /tmp

hw_ver=10
export hw_ver                                              

for i in $(seq 1 20)
do

hw_reg=0x10 #$(/tmp/ar_adc_test | grep "hw reg is " | sed "s/hw reg is //g")

case $hw_reg in
0x10)
    /tmp/boot_assist /dev/$3 /tmp/fw_memory_layout.json --dtb /tmp/artosyn-cfg-data-v10.dtb &
    hw_ver=10
    echo "########### 10 #############"                                                      
    break;
    ;;
0x40)
    value=$(devmem 0x60633058);value=$((${value} & 0xffff1cff));value=$((${value} | 0x00000400));devmem 0x60633058 32 $value
    gpio_config -e B2_7&&gpio_config -s B2_7 -d input
    /tmp/boot_assist /dev/$3 /tmp/fw_memory_layout.json --dtb /tmp/artosyn-cfg-data-v12.dtb &
    hw_ver=12
    echo "########### 40 #############"                                                                                     
    break;
    ;;
0x46)
    /tmp/boot_assist /dev/$3 /tmp/fw_memory_layout.json --dtb /tmp/artosyn-cfg-data-v11.dtb &
    hw_ver=11
    echo "########### 46 #############"                                                                                     
    break;
    ;;
0x76)
    value=$(devmem 0x60633058);value=$((${value} & 0xffff1cff));value=$((${value} | 0x00000400));devmem 0x60633058 32 $value
    gpio_config -e B2_7&&gpio_config -s B2_7 -d input
    /tmp/boot_assist /dev/$3 /tmp/fw_memory_layout.json --dtb /tmp/artosyn-cfg-data-v20.dtb &
    hw_ver=20
    echo "########### 76 #############"                                                                                     
    break;
    ;;
0x70)
    value=$(devmem 0x60633058);value=$((${value} & 0xffff1cff));value=$((${value} | 0x00000400));devmem 0x60633058 32 $value
    gpio_config -e B2_7&&gpio_config -s B2_7 -d input
    /tmp/boot_assist /dev/$3 /tmp/fw_memory_layout.json --dtb /tmp/artosyn-cfg-data-v20.dtb &
    hw_ver=211
    echo "########### 70 #############"                                                                                     
    break;
    ;;
0x73)
    value=$(devmem 0x60633058);value=$((${value} & 0xffff1cff));value=$((${value} | 0x00000400));devmem 0x60633058 32 $value
    gpio_config -e B2_7&&gpio_config -s B2_7 -d input
    /tmp/boot_assist /dev/$3 /tmp/fw_memory_layout.json --dtb /tmp/artosyn-cfg-data-v20.dtb &
    hw_ver=22
    echo "########### 73 #############"                                                                                     
    break;
    ;;
*)

    echo "### no freertos dts,refind ###"
    usleep 10000
    if [ $i == 10 ]
    then
        exit
    fi
    ;;
esac

done

#boot_assist /dev/$3 /local/usr/bin/fw_memory_layout.json &

sleep 2

# nfs setting example
#mkdir -p /nfs
#mount -t nfs -o nolock 192.168.200.6:/nfs_share/klbai /nfs
#boot_assist /nfs/a7_rtos.nonsec.img fw_memory_layout.json


#/etc/init.d/usb_gadget_configfs.sh $1
cp /usrdata/usr/data/arstack/araccess/ar_framebuffer.ko /mod -f
/etc/init.d/start_dsp.sh $1 
/etc/init.d/start_eth.sh &
cp /usrdata/usr/data/arstack/araccess/wifi/start_usbwifi.sh /tmp
cp /usrdata/usr/data/arstack/ar_wdt_service /tmp
#/tmp/ar_wdt_service -t 5 >/dev/null 2>&1 &
if [ $hw_ver == 20 -o $hw_ver == 12 -o $hw_ver == 211 -o $hw_ver == 22 ]
then
cp /usrdata/usr/data/arstack/araccess/wiegand.ko /mod -f
insmod /mod/wiegand.ko
fi

cp /usrdata/usr/data/arstack/gt9xx.ko /tmp
insmod /tmp/gt9xx.ko
cp /usrdata/usr/data/arstack/libremote_i2c.so /lib
cp /usrdata/usr/data/arstack/app30_pvt /local/usr/bin -rf

app30_pvt --detect | grep normal

if [ $? == 0 ]
then
    /usrdata/usr/data/arstack/run.sh $1
    /tmp/start_usbwifi.sh &
else
    fps=`/usrdata/usr/data/arstack/test_get_cfg /factory/web_boot.ini ISP0 fps`
    ifconfig eth0 192.168.1.100 up                                                                                                  
    cp -r /local/usr/lib/libffi.so* /lib                                                                                              
    cp -r /local/usr/lib/libgio-2.0.so* /lib                
    cp -r /local/usr/lib/libglib-2.0.so* /lib               
    cp -r /local/usr/lib/libgmodule-2.0.so* /lib            
    cp -r /local/usr/lib/libgobject-2.0.so* /lib         
    cp -r /local/usr/lib/libgthread-2.0.so* /lib         
    cp -r /local/usr/lib/libpcre.so* /lib                          
    cp -r /local/usr/lib/libpipeline_ctl.so /lib                   
    cp -r /local/usr/lib/libz.so* /lib                                                                                                           
    cp -r /local/usr/lib/libmultimedia.so /lib                                                                                                   
    cp -r /local/usr/lib/libartosyn.so /lib                                                                                                      
    cp -r /local/usr/lib/libavp_buffer.so /lib                                                                                                   
    cp -f /local/usr/bin/ipcam_service /tmp/ipcam_service                                                                                        
    cp -f /local/usr/bin/live555MediaServer /tmp/live555MediaServer                                                                              
                                                                                                                                     
    /tmp/ipcam_service --multi -t 0 -w1 1920 -h1 1080 -w2 1280 -h2 720 --fps $fps -angle 90 -index 0 --display --face_stats --sub_stream_encode 1 &
    sleep 1                                                                                                                           
    /tmp/ipcam_service --multi -t 0 -w1 1280 -h1 720 -w2 1280 -h2 720 --fps 25 -angle 90 -index 1 --face_stats --sub_stream_encode 0 &
    /tmp/live555MediaServer & 
    /usrdata/usr/data/arstack/araccess/wifi/start_usbwifi_pvt.sh
    /usrdata/usr/data/arstack/araccess/factory_test/factory_test test_lcd_back_light 50
    insmod /mod/ar_framebuffer.ko width=1024 height=600 format=0
    /usrdata/usr/data/arstack/araccess/FaceUI
fi

rtcmd cam_ser.camhw.sensor -w 3 0x1c 0 0x3a 1 0x9fe0
rtcmd cam_ser.camhw.sensor -w 3 0x1c 0 0x02 1 0x6000
rtcmd cam_ser.camhw.sensor -w 3 0x1c 0 0x1e 1 0x0000

exit

