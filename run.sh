#!/bin/sh

new_version_30=

WORK_HOME=$(dirname $0)
if [ ${WORK_HOME:0:1} == \. ]
then
    WORK_HOME=${WORK_HOME/\./$(pwd)}
fi

echo "work home: $WORK_HOME"

export LD_LIBRARY_PATH=$WORK_HOME:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/tmp:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$WORK_HOME/Linux-armv7-ceva:$LD_LIBRARY_PATH

source /etc/init.d/product_profile.sh
#source /factory/env_config.conf

cp $WORK_HOME/librsface.so /tmp/
cp $WORK_HOME/libchipid.so /tmp/
cp $WORK_HOME/arotp.ko    /tmp/
cp $WORK_HOME/shutdown.sh    /tmp/
cp $WORK_HOME/emmc_shutdown    /tmp/

insmod /tmp/arotp.ko

fps=`/usrdata/usr/data/arstack/test_get_cfg /factory/web_boot.ini ISP0 fps`

#========================================================start uvc service and hid service or ipcam_service and web

function start_process_protect(){
	
if [ $gAPP == APP_24 ]
then
	cp /usrdata/usr/data/arstack/nightwatch_sd     /tmp/nightwatch      
	#cp /usrdata/usr/data/arstack/reboot_sd.fcgi    /tmp/web/lighttpd/www/fcgi-bin/reboot.fcgi
else
	cp /usrdata/usr/data/arstack/nightwatch        /tmp/nightwatch 			
fi
			
	cp /usrdata/usr/data/arstack/ar_wdt_service 	 /tmp    
	
if [ $gAPP == APP_30 ]
then
    /tmp/nightwatch -t 60 -a /tmp/arstack.alive,/tmp/faceui.alive &
else
	/tmp/nightwatch -t 60 &
fi

if [ $gAPP != APP_24 ]
then
	/tmp/ar_wdt_service -t 5 >/dev/null 2>&1 &
fi
                            
	return 0
		
}


function uvc_service_prepare(){
	
	cp -r /local/usr/lib/libffi.so* /lib
	cp -r /local/usr/lib/libgio-2.0.so* /lib
	cp -r /local/usr/lib/libglib-2.0.so* /lib
	cp -r /local/usr/lib/libgmodule-2.0.so* /lib
	cp -r /local/usr/lib/libgobject-2.0.so* /lib
	cp -r /local/usr/lib/libgthread-2.0.so* /lib
	cp -r /local/usr/lib/libpcre.so* /lib
	cp -r /local/usr/lib/libpipeline_ctl.so /lib
	cp -r /local/usr/lib/libz.so* /lib
	cp -f /local/usr/bin/uvc_service /tmp/uvc_service
	cp  /usrdata/usr/data/arstack/test_hid_service /tmp/test_hid_service       
	
	ifconfig lo up
		
}

function ipc_service_prepare(){
	cp -r /local/usr/lib/libffi.so* /lib
	cp -r /local/usr/lib/libgio-2.0.so* /lib
	cp -r /local/usr/lib/libglib-2.0.so* /lib
	cp -r /local/usr/lib/libgmodule-2.0.so* /lib
	cp -r /local/usr/lib/libgobject-2.0.so* /lib
	cp -r /local/usr/lib/libgthread-2.0.so* /lib
	cp -r /local/usr/lib/libpcre.so* /lib
	cp -r /local/usr/lib/libpipeline_ctl.so /lib
	cp -r /local/usr/lib/libz.so* /lib
	cp -r /local/usr/lib/libmultimedia.so /lib
	cp -r /local/usr/lib/libartosyn.so /lib
	cp -r /local/usr/lib/libavp_buffer.so /lib
	cp -f /local/usr/bin/ipcam_service /tmp/ipcam_service
	#cp -f /local/usr/bin/live555MediaServer /tmp/live555MediaServer
    cp -f /usrdata/usr/data/arstack/live555MediaServer /tmp/live555MediaServer
    cp -f /usrdata/usr/data/arstack/live555MediaServer/live555_ipcam.config /tmp/live555_ipcam.config
	return 0
	
}

function start_web(){
	
	#cp -r $WORK_HOME/web /tmp/
	#cp /local/usr/lib/libartosyn.so /tmp
	#cp $WORK_HOME/libpcre.so.1 /tmp
	
	ifconfig eth0 hw ether `cat /factory/mac.cfg`
	
	$WORK_HOME/web/upgrade_service &
	$WORK_HOME/systemd /factory/web_boot.ini&
	sleep 5
	$WORK_HOME/web/lighttpd/sbin/lighttpd -m $WORK_HOME/web/lighttpd/lib -f $WORK_HOME/web/lighttpd/config/lighttpd_normal.conf
	$WORK_HOME/testWS -i /dev/icc_proxy_rpc_sink-0.1 -p 9999 2>&1 > /dev/null &
	return 0
	
}

if [ $gAPP == APP_24 -o $gAPP == APP_28 ]
then
	#dual uvc with hdr
	echo "start uvc server"
	echo "now app is $gAPP"
	
	uvc_service_prepare
	
	/tmp/uvc_service --mode uvc --fps 25 --ctl-int 2 --str-int 3 --pipe_index 1 --multi --res0 1920 1080 --res1 1280 720 --osd --angle 90 --face_stats &
	sleep 1
	/tmp/uvc_service --mode uvc --fps 30 --ctl-int 0 --str-int 1 --pipe_index 0 --multi --res0 1920 1080 --res1 1280 720 --angle 90 --face_stats &
	sleep 1
	
	echo "start hid service"
	/tmp/test_hid_service &
	
elif [ $gAPP == APP_29 ]
then
	#dual uvc with hdr
	echo "start uvc server"
	echo "now app is $gAPP"
	
	uvc_service_prepare
	
    /tmp/uvc_service --mode uvc --fps 25 --ctl-int 0 --str-int 1 -d /dev/video0 --pipe_index 1 --multi --res0 1920 1080 --res1 1280 720 --osd --angle 90 --face_stats &
	#sleep 1
	/tmp/uvc_service --mode uvc --fps 30 --ctl-int 2 --str-int 3 -d /dev/video1 --pipe_index 0 --multi --res0 1920 1080 --res1 1280 720 --angle 90 --face_stats &
	#sleep 1

	/etc/init.d/start_dsp.sh APP_29

	echo "start hid service"
	/tmp/test_hid_service &

elif [ $gAPP == APP_6 -o $gAPP == APP_7 -o $gAPP == APP_8 ]
then
	#single uvc without hdr
	echo "start uvc server"
	
	uvc_service_prepare
	source /factory/env_config.conf
	if [ $rotation == 1 ]; then
		/tmp/uvc_service --mode uvc --fps 25 --res0 1920 1080 --res1 1280 720 --osd --angle 90 --face_stats &
	else	
		/tmp/uvc_service --mode uvc --fps 25 --res0 1920 1080 --res1 1280 720 --osd --face_stats &	
	fi
	
	echo "start hid service"
	/tmp/test_hid_service &
	/etc/init.d/start_dsp.sh $gAPP

elif [ $gAPP == APP_13 ]
then
	#single uvc without hdr
	echo "start uvc server"
	
	uvc_service_prepare	
	/tmp/uvc_service -onlyfps25 --mode uvc --fps 25 --res0 3840 2160 --osd &
	
	echo "start hid service"
	/tmp/test_hid_service &

elif [ $gAPP == APP_21 ]
then
	#single ipc without hdr
	echo "start ipc server , single ipc without hdr"
	ipc_service_prepare	
	/tmp/ipcam_service --tee_copy 1 5 -n 1 -t 0 --fps 25 &
	/tmp/live555MediaServer &
	start_web

elif [ $gAPP == APP_1 -o $gAPP == APP_2 -o $gAPP == APP_4 -o $gAPP == APP_5 -o $gAPP == APP_22 ]
then
	#single ipc with hdr
	echo "start ipc server, single ipc with hdr"	
	ipc_service_prepare
	/tmp/ipcam_service --tee_copy 1 5 -n 1 -t 0 --fps 25 &
	/tmp/live555MediaServer -c /local/usr/bin/live555_xvr.config &
	start_web
elif [ $gAPP == APP_3 ]
then
    #single ipc with hdr
	echo "start ipc server, single ipc with hdr"	
	ipc_service_prepare
	/tmp/ipcam_service --tee_copy 1 5 -n 1 -t1 0 -t2 1 --fps 25 --hdr &
	/tmp/live555MediaServer -c /local/usr/bin/live555_xvr.config &
	start_web

elif [ $gAPP == APP_23 ]
then
	#dual ipc without hdr
	echo "start ipc server , dual ipc without hdr"	
	#define isp function on ceva0 : null, eis, hdr
    ceva0_isp_function=null
    camera_fps=25

	ipc_service_prepare
	echo "start ipc server 0"
    /tmp/ipcam_service --multi -t 0 -w1 1920 -h1 1080 --$ceva0_isp_function --fps $camera_fps -index 0 -angle 90&
    echo "start ipc server 1"
    /tmp/ipcam_service --multi -t 0 -w1 1920 -h1 1080 --$ceva0_isp_function --fps $camera_fps -index 1 -angle 90&
    echo "start ipc server 2"
    /tmp/ipcam_service --multi -t 0 -w1 1920 -h1 1080 --$ceva0_isp_function --fps $camera_fps -index 2 -angle 90&
    echo "start ipc server 3"
    /tmp/ipcam_service --multi -t 0 -w1 1920 -h1 1080 --$ceva0_isp_function --fps $camera_fps -index 3 -angle 90&

	/tmp/live555MediaServer &
	start_web

elif [ $gAPP == APP_10 -o $gAPP == APP_12 ]
then
	#dual ipc without hdr
	echo "start ipc server , dual ipc without hdr"	
	sed -i "/\"jpeg_mem_size\"/c\ \ \ \ \ \ \ \ \ \ \ \ \"jpeg_mem_size\" : 8388608," /local/factory/codec_cfg/codec_factory_cfg.cjson
	ipc_service_prepare
	/tmp/ipcam_service --tee_copy 1 5 -t 0 -w1 3840 -h1 2160 --fps 25 &
	/tmp/live555MediaServer -c /local/usr/bin/live555_xvr.config &
	start_web
elif [ $gAPP == APP_14 ]
then
    #dual ipc without hdr
	echo "start ipc server , dual ipc without hdr"	
	sed -i "/\"jpeg_mem_size\"/c\ \ \ \ \ \ \ \ \ \ \ \ \"jpeg_mem_size\" : 8388608," /local/factory/codec_cfg/codec_factory_cfg.cjson
	ipc_service_prepare
	/tmp/ipcam_service --tee_copy 1 5 -t1 0 -w1 3840 -h1 2160 -t2 1 --fps 25 &
	/tmp/live555MediaServer -c /local/usr/bin/live555_xvr.config &
	start_web

elif [ $gAPP == APP_26 ]
then
	#dual ipc without hdr
	echo "start ipc server , dual ipc without hdr"	
	sed -i "/\"jpeg_mem_size\"/c\ \ \ \ \ \ \ \ \ \ \ \ \"jpeg_mem_size\" : 8388608," /local/factory/codec_cfg/codec_factory_cfg.cjson
	ipc_service_prepare
	/tmp/ipcam_service --tee_copy 1 5 -t1 0 -t2 1 -w1 3840 -h1 2160 -w2 1920 -h2 1080 --fps 25 &
	/tmp/live555MediaServer -c /local/usr/bin/live555_xvr.config &
	start_web
elif [ $gAPP == APP_9 ]
then
	echo "start calc box"
	ipc_service_prepare
	cp -f /local/usr/bin/rtsp_client /tmp/rtsp_client
	cp -f /local/usr/bin/rtsp_service /tmp/rtsp_service
	/tmp/live555MediaServer &
	start_web
	
elif [ $gAPP == APP_27 -o $gAPP == APP_30 ]
then
	#dual ipc without hdr

	#start_process_protect
	#$WORK_HOME/tick_wdt.sh &
	echo "start ipc server , dual ipc without hdr"	
	ipc_service_prepare
	/tmp/ipcam_service --multi -t 0 -w1 1920 -h1 1080 -w2 1280 -h2 720 --fps $fps -angle 90 -index 0 --display --face_stats --sub_stream_encode 1 &
	sleep 1
	/tmp/ipcam_service --multi -t 0 -w1 1280 -h1 720 -w2 1280 -h2 720 --fps 25 -index 1 --face_stats --sub_stream_encode 0 &
    sleep 1
	/tmp/ipcam_service --multi -t 0 -w1 1280 -h1 720 -w2 1280 -h2 720 --fps 25 -index 2 --face_stats --sub_stream_encode 0 &
    sleep 1
    /tmp/live555MediaServer -c /tmp/live555_ipcam.config &
	#start_web
	$WORK_HOME/araccess/audio_service
elif [ $gAPP == APP_20 -o $gAPP == APP_19 ]
then
    #single ipc with hdr
	echo "start ipc server, single ipc with hdr"	
	ipc_service_prepare
	#/tmp/ipcam_service --face_stats -t 0 --tee_copy 1 5 -w1 1920 -h1 1080 -w2 640 -h2 480 --fps 25 --display -w 720 -h 576 -fps 50 &
	#/tmp/live555MediaServer &
	/tmp/ipcam_service --face_stats -t1 1 -t2 1 --tee_copy 1 5 -w1 1920 -h1 1080 -w2 1920 -h2 1080 --fps 25 --sub_stream_encode 1 &
	/tmp/live555MediaServer -c /local/usr/bin/live555_xvr.config &
	start_web
elif [ $gAPP == APP_3_UIPC ]
then
	#single ipc
	echo "start ipc server, single ipc"	
	ipc_service_prepare
	/tmp/ipcam_service --tee_copy 1 5 -n 1 -t1 0 -t2 1 --fps 25&
	/tmp/live555MediaServer -c /local/usr/bin/live555_xvr.config &
	start_web
fi

#================================================================================================run the app
#  arstack para option
#
#  -i : input source
#  -w : weights file
#  -l : license file
#  -v : log level
#  -R : don`t copy the main stream
#  -u : upload the uvc private data
#  -s : statistics information log level
#  -a : alg stream for ir sensor device node
#  -A : main stream for ir sensor device node
#  -m : alg stream for rgb sensor device node
#  -M : main stream for rgb sensor device node
#  -r : rotation angle
#  -z : start the protect process
#=================================================================================================

if [ $gAPP == APP_1 -o $gAPP == APP_2 ]
then

	#setting core voltage at 0.82V
	i2cset -f -y 0 0x58 0x0a3 0x34
	i2cget -f -y 0 0x58 0x0a3
	i2cset -f -y 0 0x58 0x0a4 0x34
	i2cget -f -y 0 0x58 0x0a4

	$WORK_HOME/arstack -b /database -i videolive -w $WORK_HOME/rsweights.conf -a "/dev/video_yuv-0.2" -A "/dev/video_yuv-0.1" -l /factory/lic.key -v 1 -z &
	start_process_protect
	
elif [ $gAPP == APP_3 -o $gAPP == APP_4 ]
then

	#setting core voltage at 0.86V
	i2cset -f -y 0 0x58 0x0a3 0x38
	i2cget -f -y 0 0x58 0x0a3
	i2cset -f -y 0 0x58 0x0a4 0x38
	i2cget -f -y 0 0x58 0x0a4

	
	$WORK_HOME/arstack -b /database -i videolive -w $WORK_HOME/rsweights.conf -a "/dev/video_yuv-0.2" -A "/dev/video_yuv-0.1" -l /factory/lic.key -v 1 -z &
	start_process_protect

elif [ $gAPP == APP_5 ]
then
	
	$WORK_HOME/arstack -b /database -a "/dev/video_yuv-0.2" -A "/dev/video_yuv-0.1" -i videolive -w $WORK_HOME/rsweights.conf -l /factory/lic.key -v 1 -z &
	start_process_protect
	
elif [ $gAPP == APP_6 ]
then

	#setting core voltage at 0.82V
	i2cset -f -y 0 0x58 0x0a3 0x34
	i2cget -f -y 0 0x58 0x0a3
	i2cset -f -y 0 0x58 0x0a4 0x34
	i2cget -f -y 0 0x58 0x0a4
	
	
	$WORK_HOME/arstack -i videolive -a "/dev/video_yuv-1" -w $WORK_HOME/rsweights.conf -l /factory/lic.key -u -v 1 -z &
	start_process_protect

elif [ $gAPP == APP_7 ]
then

	#setting core voltage at 0.82V
	i2cset -f -y 0 0x58 0x0a3 0x34
	i2cget -f -y 0 0x58 0x0a3
	i2cset -f -y 0 0x58 0x0a4 0x34
	i2cget -f -y 0 0x58 0x0a4

	if [ $rotation == 1 ]; then	
		$WORK_HOME/arstack -i videolive -a "/dev/video_yuv-1" -w $WORK_HOME/rsweights.conf -l /factory/lic.key --faceae0 /dev/cam_src-0 -u -v 1 -r 90 -z&
	else
		$WORK_HOME/arstack -i videolive -a "/dev/video_yuv-1" -w $WORK_HOME/rsweights.conf -l /factory/lic.key --faceae0 /dev/cam_src-0 -u -v 1 -z&
	fi
	start_process_protect

elif [ $gAPP == APP_8 ]
then

	#setting core voltage at 0.82V
	i2cset -f -y 0 0x58 0x0a3 0x34
	i2cget -f -y 0 0x58 0x0a3
	i2cset -f -y 0 0x58 0x0a4 0x34
	i2cget -f -y 0 0x58 0x0a4
	
	
	$WORK_HOME/arstack -m $WORK_HOME/face_alg_param.conf -e 3 -i videolive -v 0 -s 0 -u -y 1 -w $WORK_HOME/weights.conf  &
	start_process_protect

elif [ $gAPP == APP_9 ]
then

	echo "APP_9 tbd"
	start_process_protect
	
elif [ $gAPP == APP_20 -o $gAPP == APP_19 ]
then

	#setting core voltage at 0.82V
	i2cset -f -y 0 0x58 0x0a3 0x34
	i2cget -f -y 0 0x58 0x0a3
	i2cset -f -y 0 0x58 0x0a4 0x34
	i2cget -f -y 0 0x58 0x0a4
	
	
#	$WORK_HOME/arstack -b /database -i videolive -w $WORK_HOME/rsweights.conf -a "/dev/video_yuv-0.2" -A "/dev/video_yuv-0.1" -l /factory/lic.key -v 1 -z &
	start_process_protect
	#sleep 1
	#rtcmd cam_ser.camhw.sensor -g 20 1 0 3 2 1 0
	#rtcmd cam_ser.camhw.sensor -g 18 1 0 3 0 1 1
	#/usrdata/usr/data/arstack/cam_raw_test --cam_id 1 --size 512 202 --count 500000 --fps 30 --skip 90&
	#sleep 10
	#$WORK_HOME/rtsp_client -i rtsp://127.0.0.1/h264/ch01/sub/av_stream 2>&1 > /dev/null &

elif [ $gAPP == APP_13 ]
then

	#setting core voltage at 0.82V
	i2cset -f -y 0 0x58 0x0a3 0x34
	i2cget -f -y 0 0x58 0x0a3
	i2cset -f -y 0 0x58 0x0a4 0x34
	i2cget -f -y 0 0x58 0x0a4
	
	$WORK_HOME/arstack -i videolive -A "" -a "/dev/video_yuv-1" -w $WORK_HOME/rsweights.conf -l /factory/lic.key -u -v 1 -z&
	start_process_protect

elif [ $gAPP == APP_21 ]
then

	#setting core voltage at 0.82V
	i2cset -f -y 0 0x58 0x0a3 0x34
	i2cget -f -y 0 0x58 0x0a3
	i2cset -f -y 0 0x58 0x0a4 0x34
	i2cget -f -y 0 0x58 0x0a4
		
	$WORK_HOME/arstack -b /database -i videolive -w $WORK_HOME/rsweights.conf -a "/dev/video_yuv-0.2" -A "/dev/video_yuv-0.1" -l /factory/lic.key -v 1 -z&
	start_process_protect

elif [ $gAPP == APP_22 ]
then

	#setting core voltage at 0.86V
	i2cset -f -y 0 0x58 0x0a3 0x38
	i2cget -f -y 0 0x58 0x0a3
	i2cset -f -y 0 0x58 0x0a4 0x38
	i2cget -f -y 0 0x58 0x0a4
	
	
	$WORK_HOME/arstack -b /database -i videolive -w $WORK_HOME/rsweights.conf -a "/dev/video_yuv-0.2" -A "/dev/video_yuv-0.1" -l /factory/lic.key -v 1 -z&
	start_process_protect

elif [ $gAPP == APP_23 ]
then

	#setting core voltage at 0.86V
	i2cset -f -y 0 0x58 0x0a3 0x38
	i2cget -f -y 0 0x58 0x0a3
	i2cset -f -y 0 0x58 0x0a4 0x38
	i2cget -f -y 0 0x58 0x0a4
	 
	$WORK_HOME/arstack -b /database -i videolive -a "/dev/video_yuv-0.2" -A "" -m "/dev/video_yuv-1.2" -M "" -n "/dev/video_yuv-2.2" -N "" -o "/dev/video_yuv-3.2" -O ""   -w /$WORK_HOME/rsweights.conf -l /factory/lic.key -v 1 -r 90 -z&
	start_process_protect

elif [ $gAPP == APP_24 -o $gAPP == APP_28 ]
then
	#setting core voltage at 0.82V
	i2cset -f -y 0 0x58 0x0a3 0x34
	i2cget -f -y 0 0x58 0x0a3
	i2cset -f -y 0 0x58 0x0a4 0x34
	i2cget -f -y 0 0x58 0x0a4
	
	# now dwt dual camera need specific arstack
    #$WORK_HOME/arstack -i videolive -w $WORK_HOME/rsweights.conf -I "/dev/video_yuv-1-noosd-0" -a "/dev/video_yuv-1" -l /factory/lic.key -u -v 1 -z&

#faceae
    $WORK_HOME/arstack -i videolive -w $WORK_HOME/rsweights.conf -I "/dev/video_yuv-1-noosd-0" -a "/dev/video_yuv-1" -l /factory/lic.key --faceae0 /dev/cam_src-1 --infrae0 /dev/cam_src-0  -u -v 1 -r 90 -z&
	start_process_protect

elif [ $gAPP == APP_29 ]
then
	#setting core voltage at 0.82V
	i2cset -f -y 0 0x58 0x0a3 0x34
	i2cget -f -y 0 0x58 0x0a3
	i2cset -f -y 0 0x58 0x0a4 0x34
	i2cget -f -y 0 0x58 0x0a4
	ulimit -n 5120
	sleep 1
	# now dwt dual camera need specific arstack
    #$WORK_HOME/arstack -i videolive -w $WORK_HOME/rsweights.conf -I "/dev/video_yuv-1-noosd-0" -a "/dev/video_yuv-1" -l /factory/lic.key -u -v 1 -z&

#faceae
    $WORK_HOME/arstack -i videolive -w $WORK_HOME/stweights.conf -I "/dev/video_yuv-1-noosd-0" -a "/dev/video_yuv-1" -l $WORK_HOME/license.lic --faceae0 /dev/cam_src-1 --infrae0 /dev/cam_src-0  -u -v 1 -r 90 -z&
	start_process_protect
		
elif [ $gAPP == APP_10 -o $gAPP == APP_11 -o $gAPP == APP_12 -o $gAPP == APP_14 -o $gAPP == APP_26 ]
then
	
	#setting core voltage at 0.82V
	i2cset -f -y 0 0x58 0x0a3 0x34
	i2cget -f -y 0 0x58 0x0a3
	i2cset -f -y 0 0x58 0x0a4 0x34
	i2cget -f -y 0 0x58 0x0a4
	$WORK_HOME/arstack -b /database -i videolive -A "/dev/video_yuv-0.1" -a "/dev/video_yuv-0.2" -w $WORK_HOME/rsweights.conf -l /factory/lic.key -v 1 -z&
	start_process_protect
    sleep 10
	$WORK_HOME/data/progs/saveimg &
	
	#clear cached
    echo 3 > /proc/sys/vm/drop_caches
    
elif [ $gAPP == APP_27 ]
then
	#setting core voltage at 0.82V
	i2cset -f -y 0 0x58 0x0a3 0x34
	i2cget -f -y 0 0x58 0x0a3
	i2cset -f -y 0 0x58 0x0a4 0x34
	i2cget -f -y 0 0x58 0x0a4
	
    test_cam_src --name 0 --enable_mirror 1
    test_cam_src --name 1 --enable_mirror 1
	
	# now dwt dual camera need specific arstack
    $WORK_HOME/arstack -b /database -i videolive -w $WORK_HOME/rsweights.conf -a "/dev/video_yuv-0.2" -A "/dev/video_yuv-0.1" -I "/dev/video_yuv-1.1" -l /factory/lic.key --infrae0 /dev/cam_src-1 --faceae0 /dev/cam_src-0 -v 1 -r 90 -z&
	start_process_protect
	$WORK_HOME/araccess/araccess -r -b /database/ -l 1 -c /dev/ttyS2 &
	insmod /mod/ar_framebuffer.ko width=1024 height=600 format=0
	sleep 2
	source $WORK_HOME/araccess/envQt-noiconv.config
	$WORK_HOME/araccess/FaceUI &
	
elif [ $gAPP == APP_30 ]
then
	#setting core voltage at 0.86V
	i2cset -f -y 0 0x58 0x0a3 0x38
	i2cget -f -y 0 0x58 0x0a3
	i2cset -f -y 0 0x58 0x0a4 0x38
	i2cget -f -y 0 0x58 0x0a4
	
    #gpio pin_share config
    #uart1
    pin_share_config -ps 42 3
    pin_share_config -ps 43 3
    
	ulimit -n 5120
	
	rm /database/coredump* -rf
	
	sleep 1
	
    test_cam_src --name 0 --enable_flip 1
    test_cam_src --name 1 --enable_flip 1
    test_cam_src --name 2 --enable_flip 1
    
#	/tmp/ar_adc_test | grep "version_num = 0x46"&&new_version_30="-a 2"&&$WORK_HOME/araccess/factory_test/factory_test test_alarm_high_NO_v1

	if [ $hw_ver == 211 -o $hw_ver == 22 ]
	then
			new_version_30="-a 20"
	else
			new_version_30="-a $hw_ver"
	fi 

  name_ts=$(find /sys/devices/virtual/input/ -name event* | sed "s/\//\n/g" | grep event)
  name_ms=$(cat /proc/bus/input/devices | grep mouse | sed "s/\ /\n/g" | grep event)
  name_ir=$(cat /proc/bus/input/devices | grep event)

  if [ x$name_ts != x ]
  then
			name_ir=$(echo $name_ir | sed "/$name_ts/d")
  fi

  if [ x$name_ms != x ]
  then
			name_ir=$(echo $name_ir | sed "/$name_ms/d")
  fi

  name_ir=$(echo $name_ir | sed "s/\ /\n/g" | grep event)

  if [ x$name_ts == x ]
  then
       name_ts=eventx
  fi

  if [ x$name_ms == x ]
  then
      name_ms=eventy
  fi

	export MOUSE_EVENT=$name_ms                                                                                  
	export TOUCH_EVENT=$name_ts                                                                                  
	
  if [ x$name_ir != x ]
  then                 
      ir_dir=/dev/input/$name_ir
  fi 	

	# now dwt dual camera need specific arstack
  #$WORK_HOME/arstack -b /database --scanboard "$ir_dir" -i videolive -w $WORK_HOME/stweights.conf -a "/dev/video_yuv-0.2" -A "" -I "/dev/video_yuv-1.1"  -l $WORK_HOME/license.lic --crypto offline --cpti2ctype remote --cpti2cport 3 --infrae0 /dev/cam_src-1 --faceae0 /dev/cam_src-0 --thermal magcore -v 1 -r 90 -z&
	#$WORK_HOME/araccess/araccess -r -b /database/ -l 1 -c /dev/ttyS2 ${new_version_30}&
	insmod /mod/ar_framebuffer.ko width=1024 height=600 format=0
	sleep 2
	$WORK_HOME/araccess/factory_test/factory_test test_lcd_back_light 50
	source $WORK_HOME/araccess/envQt-noiconv.config
	#$WORK_HOME/araccess/FaceUI ${new_version_30}&

elif [ $gAPP == APP_3_UIPC ]
then

	#setting core voltage at 0.82V
	i2cset -f -y 0 0x58 0x0a3 0x38
	i2cget -f -y 0 0x58 0x0a3
	i2cset -f -y 0 0x58 0x0a4 0x38
	i2cget -f -y 0 0x58 0x0a4

	
	$WORK_HOME/arstack -i videolive -w $WORK_HOME/rsweights.conf -a "/dev/video_yuv-0.2" -A "/dev/video_yuv-0.1" -l /factory/lic.key -v 1 -z &
	start_process_protect
	
fi

if [ $gAPP == APP_30 -o $gAPP == APP_7 -o $gAPP == APP_13 -o $gAPP == APP_24  -o $gAPP == APP_28 -o $gAPP == APP_29 -o $gAPP == APP_9 ]
then
    echo "no onvif"
else
    for i in $(seq 1 5)
    do
        sleep 2;
        ifconfig | grep Bcast > /dev/null&&break;
    done
    
    cp -rf /usrdata/usr/data/arstack/onvif_server/ /tmp/
    cd /tmp/onvif_server && ./onvif_server &

fi

if [ $gAPP == APP_30 ]
then
    cp -rf /usrdata/usr/data/arstack/pad_access/ /tmp/
    /tmp/pad_access/ar_pad_access &
fi

if [ $gAPP == APP_3 -o $gAPP == APP_14 ]
then
    cp /usrdata/usr/data/arstack/p2p/ /tmp/ -rf
    sleep 10
    /tmp/p2p/ar_p2p_service &
fi

#cp /usrdata/usr/data/arstack/nightwatch     /tmp
#cp /usrdata/usr/data/arstack/ar_wdt_service /tmp
#/tmp/ar_wdt_service -t 5 >/dev/null 2>&1 &
#/tmp/nightwatch -t 60 &

# uvc need set mirror
#if [ $1 == APP_6 -o $gAPP == APP_7 -o $gAPP == APP_13 ]
#then
#	sleep 15
#	test_cam_src --enable_mirror 1
#fi

exit
