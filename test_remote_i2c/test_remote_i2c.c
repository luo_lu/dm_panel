#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include "ar_log.h"


#include "backtrace.h"

#include "libremote_i2c.h"
#include "test_remote_i2c.h"


#include "ar_icc.h"
/*
#include <asm/io.h>
#include <linux/kernel.h>
#include <linux/mm.h>
#include <linux/mman.h>
#include <linux/uaccess.h>
#include <linux/pagemap.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/proc_fs.h>
#include <linux/dma-mapping.h>
#include<linux/err.h>
*/


///////////////////////////////////////////////

int main(int argc, char const *argv[])
{
	unsigned int test_addr = 0x3002;
    unsigned int test_data = 0x01;

    unsigned char data_write[3];
    unsigned char data_read[3];
	
	if (argc <= 1){
			printf("%s w|r|rkey slave_addr reg_addr [reg_data] i2c_dev\n", argv[0]);
			return -1;
	}
	
	
    /* code */
    back_trace_t *back_trace=creat_back_trace();

    unsigned int i2c_id_7bit = 0x60;
    unsigned int fd = remote_i2c_open(DEV_REMOTE_I2C_PATH);
    if(0 == fd)
    {
        printf("fail to open %s, exit...\n", DEV_REMOTE_I2C_PATH);
        return -1;
    }

    printf("success to open %s : 0x%x\n",DEV_REMOTE_I2C_PATH,fd);

	
	if (strcmp(argv[1], "r") == 0){
		sscanf(argv[2],"0x%x",&i2c_id_7bit);
		printf("i2c_id_7bit %x\n",i2c_id_7bit);
		sscanf(argv[3], "0x%x", &test_addr);
        printf("get addr [0x%x]\n", test_addr);
		
		data_write[0]=(test_addr>>8) & 0xFF;
	    data_write[1]=(test_addr) & 0xFF;

        if(remote_i2c_config(fd, (REMOTE_I2C_COMPONENT_Enum)atoi(argv[4]), REMOTE_I2C_MASTER_MODE, i2c_id_7bit, REMOTE_I2C_STANDARD_SPEED))
        {
            printf("fail to config, exit...\n");
            return -1;
        }

	    if(remote_i2c_write_read_ext(fd, data_write, 2 , data_read, 1) )
	    {
	        printf("fail to write_read_ext, exit...\n");
	        return -1;
	    }

	   // printf("write_read_ext addr 0x%x  val:[0x%x][0x%x][0x%x] finished\n", test_addr, data_read[0], data_read[1], data_read[2]);	
        printf("write_read_ext addr 0x%x  val:[0x%x] finished\n", test_addr, data_read[0]);
		

	    return 0;
	}
	else if (strcmp(argv[1], "rkey") == 0){
		sscanf(argv[2],"0x%x",&i2c_id_7bit);
		printf("i2c_id_7bit %x\n",i2c_id_7bit);
		sscanf(argv[3], "0x%x", &test_addr);
        printf("get addr [0x%x]\n", test_addr);
		
		//data_write[0]=(test_addr>>8) & 0xFF;
	    data_write[0]=(test_addr) & 0xFF;

        if(remote_i2c_config(fd, (REMOTE_I2C_COMPONENT_Enum)atoi(argv[4]), REMOTE_I2C_MASTER_MODE, i2c_id_7bit, REMOTE_I2C_STANDARD_SPEED))
        {
            printf("fail to config, exit...\n");
            return -1;
        }

	    if(remote_i2c_write_read_ext(fd, data_write, 1 , data_read, 1) )
	    {
	        printf("fail to write_read_ext, exit...\n");
	        return -1;
	    }

	   // printf("write_read_ext addr 0x%x  val:[0x%x][0x%x][0x%x] finished\n", test_addr, data_read[0], data_read[1],      data_read[2]);	
        printf("write_read_ext addr 0x%x  val:[0x%x] finished\n", test_addr, data_read[0]);
	    return 0;
	}
	else if (strcmp(argv[1], "w") == 0){
		if (argc != 6){
			printf("%s w|r slave_addr reg_addr reg_data i2c_dev\n", argv[0]);
			return -1;
		}
		sscanf(argv[2],"0x%x",&i2c_id_7bit);
		printf("i2c_id_7bit %x\n",i2c_id_7bit);
		sscanf(argv[3], "0x%x", &test_addr);
        printf("get addr [x%x]\n", test_addr);
        sscanf(argv[4], "0x%x", &test_data);
		printf("get data [x%x]\n", test_data);
		
	    data_write[0]=(test_addr>>8) & 0xFF;
	    data_write[1]=(test_addr) & 0xFF;
	    data_write[2]=(test_data) & 0xFF;
        if(remote_i2c_config(fd, (REMOTE_I2C_COMPONENT_Enum)atoi(argv[4]), REMOTE_I2C_MASTER_MODE, i2c_id_7bit, REMOTE_I2C_STANDARD_SPEED))
        {
            printf("fail to config, exit...\n");
            return -1;
        }
	    if(remote_i2c_write(fd, data_write, 3) )
	    {
	        printf("fail to write, exit...\n");
	        return -1;
	    }

	    printf("write 0x%x to 0x%x finished\n", test_addr,test_data);

	    data_write[0]=(test_addr>>8) & 0xFF;
	    data_write[1]=(test_addr) & 0xFF;
	    if(remote_i2c_write_read_ext(fd, data_write, 2 , data_read, 1) )
	    {
	        printf("fail to write_read_ext, exit...\n");
	        return -1;
	    }

	   // printf("write_read_ext 0x%x to 0x%x 0x%x 0x%x finished\n", test_addr, data_read[0], data_read[1], data_read[2]);	
	    printf("write_read_ext 0x%x to 0x%x finished\n", test_addr, data_read[0]);	
			
		return 0;
	}
	else{
		printf("%s w|r addr [data]\n", argv[0]);
		return 0;
	}

    if(remote_i2c_close(fd))
    {
        printf("fail to close 0x%x, exit...\n",fd);
        return -1;
    }

    printf("test over, exit...\n");
    return 0;
}

