rm $1/firmwares/cam_bb_fw/library/ar_display/interface/dvp/.depend
cp .product_profile $1/
cp artosyn-cfg-data-v10.dts $1/app/artosyn/usr_app/dtb/artosyn-cfg-data-v10.dts
#cp ipcam_service/main.c $1/app/artosyn/usr_app/ipcam_service/main.c
#cp ipcam_service/ipcam.c $1/app/artosyn/usr_app/ipcam_service/src/ipcam.c
cp run.sh $1//app/artosyn/usr_data/arstack/run.sh
cp artosyn-sirius-evb.dts $1/configs/IPC_4/linux/dts/artosyn-sirius-evb.dts
cp imx307.c $1/firmwares/cam_bb_fw/library/ar_cam/camera_server/harware/peripherals/sensor/lib/imx307/imx307.c
cp test_pwm $1/app/artosyn/usr_test/ -R
cp pin_share_config  $1/app/artosyn/usr_app/ -R
#cp dm_app30_pvt  $1/app/artosyn/usr_app/ -R
cp test_vcm  $1/app/artosyn/usr_test/ -R
cp tof  $1/app/artosyn/usr_app/ -R
cp usr_lib/libar_i2c.so $1/prebuild/staging/target/lib/
cp usr_lib/libi2c.h $1/prebuild/staging/target/include/
if [ -d $1/output/staging/target ]; then
	cp usr_lib/libar_i2c.so $1/output/staging/target/lib/
	cp usr_lib/libi2c.h $1/output/staging/target/include/
fi
cp start_APP_30.sh $1/app/artosyn/usr_data/arstack/start_APP_30.sh
cp live555/* $1/app/artosyn/usr_data/arstack/
cp test_remote_i2c $1/app/artosyn/usr_test/ -R
#cp test_audio $1/app/artosyn/usr_test/ -R
cp test_uart $1/app/artosyn/usr_test/ -R
cp test_id $1/app/artosyn/usr_test/ -R
#cp start_eth.sh $1/app/artosyn/rootfs_skel/nodes/etc/init.d/start_eth.sh
