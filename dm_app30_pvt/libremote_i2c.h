/****************************************************************************
 * Copyright (C) 2019 Shanghai Artosyn Microelectronics Limited.            *
 ****************************************************************************/

/**
 * @file libremote_i2c.h
 * @author Artosyn
 * @date 8 May 2019
 * @brief File containing the APIs to do i2c operation.
 *        Users can refer to app/artosyn/usr_test/test_remote_i2c/test_remote_i2c.c for sample code, or contact Artosyn for help.
 * @example <app/artosyn/usr_test/test_remote_i2c/test_remote_i2c.c>
 */

#ifndef __LIB_REMOTE_I2C_H__
#define __LIB_REMOTE_I2C_H__

///////////////////////////////////////////////

/**
 * @brief This is the supported remote i2c component
 */
typedef enum
{
    REMOTE_I2C_COMPONENT_0 = 0, /**< remote i2c component 0 */
    REMOTE_I2C_COMPONENT_1, /**< remote i2c component 1 */
    REMOTE_I2C_COMPONENT_2, /**< remote i2c component 2 */
    REMOTE_I2C_COMPONENT_3, /**< remote i2c component 3 */
    REMOTE_I2C_COMPONENT_4, /**< remote i2c component 4 */
    REMOTE_I2C_COMPONENT_MAX /**< Reserved */
} REMOTE_I2C_COMPONENT_Enum;

/**
 * @brief This is the supported remote i2c mode
 */
typedef enum
{
    REMOTE_I2C_MASTER_MODE = 0, /**< master mode */
    REMOTE_I2C_SLAVE_MODE, /**< Reserved */
    REMOTE_I2C_MODE_MAX /**< Reserved */
} REMOTE_I2C_MODE_Enum;

/**
 * @brief This is the supported remote i2c speed
 */
typedef enum
{
    REMOTE_I2C_STANDARD_SPEED = 0,
    REMOTE_I2C_FAST_SPEED,
    REMOTE_I2C_HIGH_SPEED,
    REMOTE_I2C_SPEED_MAX /**< Reserved */
} REMOTE_I2C_SPEED_Enum;

///////////////////////////////////////////////

/**
 * @brief This function is used to open remote i2c device
 *
 *
 * @param  name  The name is the ar_gui device name string which is something like "/dev/remote_i2c".
 *
 * @retval     0         fail
 * @retval     others    success to open and the returned value is the device handle
 *
 *
 * @note null.
*/
unsigned int remote_i2c_open(const char *name);

/**
 * @brief      This function is used to close the device opened by the function \ref remote_i2c_open "remote_i2c_open".
 *
 * @param  fd    The remote device device handle returned by the function \ref remote_i2c_open "remote_i2c_open"
 *
 * @retval     0 success
 * @retval    <0 fail
 *
 * @note
 *
 */
int remote_i2c_close(unsigned int fd);

/**
 * @brief  This funciton is used to configure remote i2c component
 *
 * @param  fd    The remote device device handle returned by the function \ref remote_i2c_open "remote_i2c_open"
 * @param  i2c_component    specify remote i2c component, refer to \ref REMOTE_I2C_COMPONENT_Enum "REMOTE_I2C_COMPONENT_Enum" for details
 * @param  i2c_mode     i2c component mode(only support master mode now), refer to \ref REMOTE_I2C_MODE_Enum "REMOTE_I2C_MODE_Enum" for details
 * @param  i2c_addr_7bit  i2c 7bit address
 * @param  i2c_speed  specify remote i2c speed, refer to \ref REMOTE_I2C_SPEED_Enum "REMOTE_I2C_SPEED_Enum" for details
 *
 * @retval     0 success
 * @retval    <0 fail
 *
 * @note
 *
*/
int remote_i2c_config(unsigned int fd, unsigned int i2c_component, unsigned int i2c_mode, unsigned int i2c_addr_7bit, unsigned int i2c_speed);

/**
 * @brief  i2c write data only
 *
 * @param  fd    The remote device device handle returned by the function \ref remote_i2c_open "remote_i2c_open"
 * @param  p_data    the buffer pointer of the data to write
 * @param  data_size    the buffer length(byte) of p_data
 *
 * @retval     0 success
 * @retval    <0 fail
 *
 * @note
 *
*/
int remote_i2c_write(unsigned int fd, unsigned char *p_data, unsigned int data_size);

/**
 * @brief  i2c read data only
 *
 * @param  fd    The remote device device handle returned by the function \ref remote_i2c_open "remote_i2c_open"
 * @param  p_data    the buffer pointer of the returned data
 * @param  data_size    the buffer length(byte) of p_data
 *
 * @retval     0 success
 * @retval    <0 fail
 *
 * @note null.
*/
int remote_i2c_read(unsigned int fd, unsigned char *p_data, unsigned int data_size);

/**
 * @brief  i2c write data then read data
 *
 * @param  fd    The remote device device handle returned by the function \ref remote_i2c_open "remote_i2c_open"
 * @param  p_write    the buffer pointer of the data to write
 * @param  write_size    the buffer length(byte) of p_write
 * @param  p_read    the buffer pointer of the returned data
 * @param  read_size    the buffer length(byte) of read_size
 *
 * @retval     0 success
 * @retval    <0 fail
 *
 * @note null.
*/
int remote_i2c_write_read_ext(unsigned int fd\
    , unsigned char *p_write, unsigned int write_size \
    , unsigned char *p_read, unsigned int read_size);

///////////////////////////////////////////////

#endif //__SIRIUS_LIBI2C_H__

